import * as React from 'react';
import { List, Datagrid, Edit, Create, TabbedForm, FormTab, TextField, SelectField, EditButton, TextInput, PasswordInput, DateInput, SelectInput, required } from 'react-admin';
import PersonIcon from '@material-ui/icons/Person';

import Toolbar from '../components/toolbar';

const ViewTitle = ({ record }) => {
	return <span>Usuário {record ? `#${record.id}` : ''}</span>;
};

const roleValues = [
	{ id: 'root', name: 'Admin' },
	{ id: 'user', name: 'Padrão' },
];

const resource = {
	name: 'user',
	options: {
		label: 'Usuários',
	},
	icon: PersonIcon,
	list: (props) => (
		<List {...props} bulkActionButtons={false}>
			<Datagrid>
				<TextField label='#' source='id' sortable={false} />
				<TextField label='E-mail' source='email' sortable={false} />
				<TextField label='Nome' source='person.name' sortable={false} />
				<SelectField label='Permissão' source='role' choices={roleValues} />
				{/* <FunctionField label='Vendas' render={u => <ListButton label='Vendas' basePath={`/sale?filter={"userId":${u.id}}`} />} /> */}
				<EditButton basePath='/user' />
			</Datagrid>
		</List>
	),
	edit: (props) => (
		<Edit title={<ViewTitle />} {...props}>
			<TabbedForm toolbar={<Toolbar />}>
				<FormTab label='Usuário'>
					<TextInput disabled source='id' />
					<SelectInput label='Permissão' source='role' choices={roleValues} validate={[required()]} />
					<TextInput label='E-mail' source='email' validate={[required()]} />
					<PasswordInput label='Senha' source='password' />
					<TextInput label='Nome' source='person.name' />
				</FormTab>

				<FormTab label='Pessoal'>
					<TextInput label='Sexo' source='person.sex' />
					<TextInput label='RG' source='person.rg' />
					{/* <TextInput label='RG info' source='person.rgInfo' /> */}
					<TextInput label='CPF' source='person.cpf' />
					<TextInput label='Nacionalidade' source='person.nationality' />
					<DateInput label='Data de nascimento' source='person.birthday' />
					<TextInput label='Estado civil' source='person.maritalStatus' />
					<TextInput label='Profissão' source='person.profission' />
					<TextInput label='Telefone' source='person.phone' />
				</FormTab>

				<FormTab label='Endereço'>
					<TextInput label='CEP' source='person.address.cep' />
					<TextInput label='Estado' source='person.address.uf' />
					<TextInput label='Cidade' source='person.address.city' />
					<TextInput label='Bairro' source='person.address.district' />
					<TextInput label='Rua' source='person.address.street' />
				</FormTab>
			</TabbedForm>
		</Edit>
	),
	create: (props) => (
		<Create title='Criar usuário' {...props}>
			<TabbedForm>
				<FormTab label='Usuário'>
					<SelectInput label='Permissão' source='role' choices={roleValues} validate={[required()]} />
					<TextInput label='E-mail' source='email' validate={[required()]} />
					<PasswordInput label='Senha' source='password' validate={[required()]} />
					<TextInput label='Nome' source='person.name' />
				</FormTab>

				<FormTab label='Pessoal'>
					<TextInput label='Sexo' source='person.sex' />
					<TextInput label='RG' source='person.rg' />
					{/* <TextInput label='RG info' source='person.rgInfo' /> */}
					<TextInput label='CPF' source='person.cpf' />
					<TextInput label='Nacionalidade' source='person.nationality' />
					<DateInput label='Data de nascimento' source='person.birthday' />
					<TextInput label='Estado civil' source='person.maritalStatus' />
					<TextInput label='Profissão' source='person.profission' />
					<TextInput label='Telefone' source='person.phone' />
				</FormTab>

				<FormTab label='Endereço'>
					<TextInput label='CEP' source='person.address.cep' />
					<TextInput label='Estado' source='person.address.uf' />
					<TextInput label='Cidade' source='person.address.city' />
					<TextInput label='Bairro' source='person.address.district' />
					<TextInput label='Rua' source='person.address.street' />
				</FormTab>
			</TabbedForm>
		</Create>
	),
};

export default resource;
